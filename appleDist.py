def applesDistribution(apples, boxCapacity, maxResidue):
	arrValidSols = []
	bc = boxCapacity
	while bc >= 1:
		boxes = apples // bc
		rem = apples % bc
		if rem <= maxResidue:
			string = str(boxes) + " boxes " + str(bc) + " per box with " + str(rem) + " remaining."
			arrValidSols.append(string)
		bc -= 1
	sols = len(arrValidSols)
	print "Solutions (", sols, "):"
	for sol in arrValidSols:
		print sol
	return sols